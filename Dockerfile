FROM python:3.9-slim

ENV REDIS_HOST planet-express-charts-redis-master
ENV REDIS_PORT 6379
ENV RABBITMQ_HOST planet-express-charts-rabbitmq
ENV RABBITMQ_PORT 5672
ENV RABBITMQ_QUEUE items

RUN apt-get update
RUN apt-get -y install gcc
RUN rm -rf /var/lib/apt/lists/*

ADD requirements.txt /crawlers/requirements.txt
RUN pip install -r /crawlers/requirements.txt

COPY src/crawlers /crawlers
