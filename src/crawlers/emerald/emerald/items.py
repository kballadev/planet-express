# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html
from scrapy import Item, Field
from scrapy.loader.processors import MapCompose


class BaseItem(Item):
    url = Field()
    scraped = Field()
    updated = Field()


def get_last_element(value):
    return value[-1] if isinstance(value, list) else value

class EmeraldItem(BaseItem):
    autor = Field()
    date_time = Field(output_processor=MapCompose(get_last_element))
    title = Field()
    content = Field()
