# -*- coding: utf-8 -*-
from scrapy.linkextractors import LinkExtractor
from scrapy.spiders import CrawlSpider, Rule

from emerald.loaders import emerald_loader


class EmeraldSpider(CrawlSpider):
    name = 'emerald'
    allowed_domains = ['emerald.com']
    start_urls = ['https://www.emerald.com/insight/publication/issn/0309-0566']
    custom_settings = {
        'RABBITMQ_QUEUE': 'emerald',
    }
    rules = (
        Rule(LinkExtractor(allow=r'.*/insight/publication/issn/\d+-\d+/vol/\d+/iss/.*'), follow=True),                                    
        Rule(LinkExtractor(allow=r'.*/insight/content/doi/\d+\.\d+/\w+[\-0-9]+/full/html'), callback='parse_item', follow=False)
    )

    def parse_item(self, response):
        return emerald_loader(response)
