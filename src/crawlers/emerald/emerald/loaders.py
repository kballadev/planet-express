import logging

from scrapy.loader import ItemLoader
from scrapy.loader.processors import TakeFirst, Join
from emerald.items import EmeraldItem


logger = logging.getLogger()


class EmeraldLoader(ItemLoader):
    default_output_processor = TakeFirst()
    content_out = Join()
    autor_out = Join(';')
    date_time_out = Join(';')


def emerald_loader(response):
    
    item = EmeraldLoader(
        item=EmeraldItem(),
        response=response)
    item.add_value('url', response.url)
    autors_names = response.xpath('//span[@class="given-names"]/text()')
    autors_surnames = response.xpath('//span[@class="surname"]/text()')
    names_surnames = []
    
    for autor,surname in zip(autors_names,autors_surnames):
        names_surnames.append('{} {}'.format(autor.extract(), surname.extract()))

    item.add_value('autor', ';'.join(names_surnames))

    date_times = []
    xpath_datetime = response.xpath('//span[@class="intent_journal_publication_date"]/text()')
    for date_time in xpath_datetime:
        date_time_re = date_time.re(r'\d{1,2} \w+ \d{4}')
        if date_time_re:
            date_times.append(date_time_re[0])

    if date_times:
        item.add_value('date_time', date_times)

    item.add_xpath('title', '//h1[@class="content-title intent_article_title mt-0 mb-3"]/text()')
    item.add_xpath('content','//section[@class="intent_sub_content Abstract__block__text"]/p/text()')
    return item.load_item()