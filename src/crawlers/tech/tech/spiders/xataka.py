# -*- coding: utf-8 -*-
from scrapy.linkextractors import LinkExtractor
from scrapy.spiders import CrawlSpider, Rule

from tech.loaders import xataka_loader


class XatakaSpider(CrawlSpider):
    name = 'xataka'
    allowed_domains = ['xataka.com']
    start_urls = ['https://www.xataka.com/categoria/inteligencia-artificial']
    custom_settings = {
        'RABBITMQ_QUEUE': 'xataka',
    }
    rules = (
        Rule(LinkExtractor(allow=r'.*/inteligencia-artificial/record/\d+'), follow=True),
        Rule(LinkExtractor(allow=r'.*/inteligencia-artificial/\w+-.*'), callback='parse_item', follow=True)
    )

    def parse_item(self, response):
        return xataka_loader(response)
