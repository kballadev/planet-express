# -*- coding: utf-8 -*-
from scrapy.linkextractors import LinkExtractor
from scrapy.spiders import CrawlSpider, Rule

from tech.loaders import genbeta_loader


class GenbetaSpider(CrawlSpider):
    name = 'genbeta'
    allowed_domains = ['genbeta.com']
    start_urls = ['https://www.genbeta.com/tag/desarrollo/record/20']
    custom_settings = {
        'RABBITMQ_QUEUE': 'genbeta',
    }
    rules = (
        Rule(LinkExtractor(allow=r'.*/tag/desarrollo/record/\d+'), follow=True),
        Rule(LinkExtractor(allow=r'.*/desarrollo/\w+-.*'), callback='parse_item', follow=True)
    )

    def parse_item(self, response):
        return genbeta_loader(response)
