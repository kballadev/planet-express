# -*- coding: utf-8 -*-
from scrapy.linkextractors import LinkExtractor
from scrapy.spiders import CrawlSpider, Rule

from tech.loaders import gizmodo_loader


class GizmodoSpider(CrawlSpider):
    name = 'gizmodo'
    allowed_domains = ['gizmodo.com']
    start_urls = ['https://es.gizmodo.com/c/tecnologia?startIndex=20']
    custom_settings = {
        'RABBITMQ_QUEUE': 'gizmodo',
    }
    rules = (
        Rule(LinkExtractor(allow=r'.*es.gizmodo.com/c/tecnologia?startIndex=\d+'), follow=True),
        Rule(LinkExtractor(allow=r'.*es.gizmodo.com/\w+-.*'), callback='parse_item', follow=True)
    )

    def parse_item(self, response):
        return gizmodo_loader(response)
