import logging

from scrapy.loader import ItemLoader
from scrapy.loader.processors import TakeFirst, Join

from tech.items import XatakaItem, GenbetaItem, GizmodoItem


logger = logging.getLogger()


class XatakaLoader(ItemLoader):
    default_output_processor = TakeFirst()
    content_out = Join()


def xataka_loader(response):
    item = XatakaLoader(
        item=XatakaItem(),
        response=response)
    item.add_value('url', response.url)
    item.add_xpath('autor', '//a[@class="article-author-link"]/text()')
    item.add_xpath('date_time', '//time[@class="article-date"]/text()')
    item.add_xpath('title', '//article/div/header/h1/span/text()')
    item.add_xpath('content',
                   '//div[@class="article-content"]//text()')
    return item.load_item()

class GenbetaLoader(ItemLoader):
    default_output_processor = TakeFirst()
    content_out = Join()


def genbeta_loader(response):
    item = GenbetaLoader(
        item=GenbetaItem(),
        response=response)
    item.add_value('url', response.url)
    item.add_xpath('autor', '//a[@class="article-author-link"]/text()')
    item.add_xpath('date_time', '//time[@class="article-date"]/text()')
    item.add_xpath('title', '//article/div/header/h1/span/text()')
    item.add_xpath('content',
                   '//div[@class="article-content"]//text()')
    return item.load_item()


class GizmodoLoader(ItemLoader):
    default_output_processor = TakeFirst()
    content_out = Join()


def gizmodo_loader(response):
    item = GizmodoLoader(
        item=GizmodoItem(),
        response=response)
    item.add_value('url', response.url)
    item.add_xpath('autor', '//body/div/div/main/div/div[@class="js_starterpost"]/div/div/div/div/div/div/div/a/text()')
    item.add_xpath('date_time', '//body/div/div/main/div/div[@class="js_starterpost"]/div/div/div/div/div/div/time/a/text()')
    item.add_xpath('title', '//body/div/div/div/header/h1/text()')
    item.add_xpath('content',
                   '//body/div/div/main/div/div[@class="js_starterpost"]/div[contains(@class, "js_post-content")]//text()')
    return item.load_item()