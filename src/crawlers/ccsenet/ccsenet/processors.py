
class TakeLast:

    """
    Returns the Last non-null/non-empty value from the values received,
    so it's typically used as an output processor to single-valued fields.
    It doesn't receive any ``__init__`` method arguments, nor does it accept Loader contexts.

    Example:

    >>> from itemloaders.processors import TakeLast
    >>> proc = TakeLast()
    >>> proc(['', 'one', 'two', 'three'])
    'three'
    """


    def __call__(self, values):
        return values[-1] if isinstance(values, list) and values else values
          