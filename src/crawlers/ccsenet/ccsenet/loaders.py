import logging

from scrapy.loader import ItemLoader
from scrapy.loader.processors import TakeFirst, Join
from ccsenet.items import CcsenetItem
from ccsenet.processors import TakeLast


logger = logging.getLogger()


class CcsenetLoader(ItemLoader):
    default_output_processor = TakeFirst()
    date_time_out = TakeLast()
    content_out = Join()
    autor_out = Join(';')


def ccsenet_loader(response):
    item = CcsenetLoader(
        item=CcsenetItem(),
        response=response)
    item.add_value('url', response.url)
    item.add_xpath('autor', '//span[@class="small"]/text()')

    date_times = []
    xpath_datetime = response.xpath('//ul[@class="breadcrumb"]/li/a/text()')
    for date_time in xpath_datetime:
        date_time_re = date_time.re(r'\d{4}')
        if date_time_re:
            date_times.append(date_time_re[0])

    if date_times:
        item.add_value('date_time', date_times)

    item.add_xpath('title', '//div[@class="col-sm-12 col-md-12 col-lg-12 col-xl-12"]/h3/text()')
    content = response.xpath('//div[@class="col-sm-12 col-md-12 col-lg-12 col-xl-12"]/div/p/text()')
    item.add_xpath('content', '//div[@class="col-sm-12 col-md-12 col-lg-12 col-xl-12"]/div/p/text()')

    return item.load_item() if content else None
