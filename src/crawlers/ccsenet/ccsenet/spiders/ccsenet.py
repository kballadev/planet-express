# -*- coding: utf-8 -*-
from scrapy.linkextractors import LinkExtractor
from scrapy.spiders import CrawlSpider, Rule

from ccsenet.loaders import ccsenet_loader


class CcsenetSpider(CrawlSpider):
    name = 'ccsenet'
    allowed_domains = ['ccsenet.org']
    start_urls = ['http://www.ccsenet.org/journal/index.php/ijms/issue/archives']
    custom_settings = {
        'RABBITMQ_QUEUE': 'ccsenet',
    }
    rules = (
        Rule(LinkExtractor(allow=r'.*/journal/index.php/ijms/issue/view/.+'), follow=True),                                    
        Rule(LinkExtractor(allow=r'.*/journal/index.php/ijms/article/view/.+'), callback='parse_item', follow=False)
    )

    def parse_item(self, response):
        return ccsenet_loader(response)
