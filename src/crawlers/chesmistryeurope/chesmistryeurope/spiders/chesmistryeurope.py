# -*- coding: utf-8 -*-
from scrapy.linkextractors import LinkExtractor
from scrapy.spiders import CrawlSpider, Rule

from chesmistryeurope.loaders import chesmistryeurope_loader


class ChesmistryEuropeSpider(CrawlSpider):
    name = 'chesmistryeurope'
    allowed_domains = ['chemistry-europe.onlinelibrary.wiley.com']
    start_urls = ['https://chemistry-europe.onlinelibrary.wiley.com/journal/21911363']
    custom_settings = {
        'RABBITMQ_QUEUE': 'chesmistryeurope',
    }
    rules = (
        Rule(LinkExtractor(allow=r'.*chemistry-europe.onlinelibrary.wiley.com/doi/(abs|full)/\d+\.\d+/.*\..*'), callback='parse_item', follow=True),
        Rule(LinkExtractor(allow=r'.*chemistry-europe.onlinelibrary.wiley.com/doi/\d+\.\d+/.*\..*'), callback='parse_item', follow=True),
        Rule(LinkExtractor(
                deny=[
                    r'.*/doi/pdf.*',
                    r'.*/doi/epdf.*',
                    r'.*/pb-assets.*',
                    r'.*/action.*',
                    r'.*/help.*',
                    r'.*/search.*',
                    r'.*/feedback.*',
                    r'.*/rss.*',
                    r'.*/page/account-confirmation-thanks.*',
                    r'.*/media.*',
                    r'.*/medical-research.*',
                    r'.*/servlet/linkout.*',
                    r'.*/na101/.*',
                    r'.*/na101v1/.*',
                    r'.*/na102/.*',
                    r'.*/doi/mlt/.*',
                    r'.*/topic.*',
                    r'.*/author/.*']),
                follow=True)
    )

    def parse_item(self, response):
        return chesmistryeurope_loader(response)
