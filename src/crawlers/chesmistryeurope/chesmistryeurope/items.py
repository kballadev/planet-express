# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html
from scrapy import Item, Field
from scrapy.loader.processors import MapCompose


class BaseItem(Item):
    url = Field()
    scraped = Field()
    updated = Field()


class ChesmistryEuropeItem(BaseItem):
    autor = Field()
    date_time = Field()
    title = Field()
    content = Field()
