import logging

from scrapy.loader import ItemLoader
from scrapy.loader.processors import TakeFirst, Join
from chesmistryeurope.items import ChesmistryEuropeItem


logger = logging.getLogger()


class ChesmistryEuropeLoader(ItemLoader):
    default_output_processor = TakeFirst()
    content_out = Join()
    autor_out = Join(';')


def chesmistryeurope_loader(response):
    
    item = ChesmistryEuropeLoader(
        item=ChesmistryEuropeItem(),
        response=response)
    item.add_value('url', response.url)

    item.add_xpath('autor', '//div[@class="loa-wrapper loa-authors hidden-xs"]/div/div/div/a/span/text()')
    item.add_xpath('date_time', '//span[@class="epub-date"]/text()')

    item.add_xpath('title', '//h1[@class="citation__title"]/text()')
    item.add_xpath('content','//div[@class="article-section__content en main"]/p/text()')

    item_loaded = item.load_item()
    return item_loaded if item_loaded.get('content') else None