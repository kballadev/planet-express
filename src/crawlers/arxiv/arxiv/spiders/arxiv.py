# -*- coding: utf-8 -*-
from scrapy.linkextractors import LinkExtractor
from scrapy.spiders import CrawlSpider, Rule

from arxiv.loaders import arxiv_loader


class MathsSpider(CrawlSpider):
    name = 'arxiv'
    allowed_domains = ['arxiv.org']
    start_urls = ['https://arxiv.org/']
    custom_settings = {
        'RABBITMQ_QUEUE': 'arxiv',
    }
    rules = (
        Rule(LinkExtractor(allow=r'.*/archive/\w+'), follow=True),
        Rule(LinkExtractor(allow=r'.*/year/\w+/\d+'), follow=True),
        Rule(LinkExtractor(allow=r'.*/list/\w+/\d+'), follow=True),
        Rule(LinkExtractor(allow=r'.*/list/\w+/\d+\?skip=\d+&show=\d+'), follow=True),
        Rule(LinkExtractor(allow=r'.*/abs/\d+\.\d+'), callback='parse_item', follow=False)
    )

    def parse_item(self, response):
        return arxiv_loader(response)
