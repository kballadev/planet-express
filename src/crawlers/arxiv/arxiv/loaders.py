import logging

from scrapy.loader import ItemLoader
from scrapy.loader.processors import TakeFirst, Join

from arxiv.items import ArxivItem


logger = logging.getLogger()


class ArxivLoader(ItemLoader):
    title_out = TakeFirst()
    url_out = TakeFirst()
    content_out = Join()
    autor_out = Join(';')
    date_time_out = Join(';')



def arxiv_loader(response):
    item = ArxivLoader(
        item=ArxivItem(),
        response=response)
    item.add_value('url', response.url)
    item.add_xpath('autor', '//div[@class="authors"]/a/text()')

    date_times = []
    xpath_datetime = response.xpath('//div[@class="submission-history"]/text()')
    for date_time in xpath_datetime:
        date_time_re = date_time.re(r'\d{1,2} \w{3} \d{4} \d{2}:\d{2}:\d{2} \w+')
        if date_time_re:
            date_times.append(date_time_re[0])

    if date_times:
        item.add_value('date_time', date_times)

    item.add_xpath('title', '//h1[@class="title mathjax"]/text()')
    item.add_xpath('content', '//blockquote[@class="abstract mathjax"]//text()')
    return item.load_item()
